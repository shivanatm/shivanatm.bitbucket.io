# The Best Savings Account
<!-- BOQ Image --> 
```{r boq-logo, echo=FALSE}
knitr::include_graphics("https://702rc195vu6fsa4x3pnb6d8m-wpengine.netdna-ssl.com/wp-content/uploads/2015/11/nQRmMa1.jpg")
```

The best savings account we found is the [Bank of Queensland Fast Track Saver Account](https://www.boq.com.au/personal/banking/savings-and-term-deposits/fast-track-saver-account).

It has a 2.15% interest rate for balances up to $250,000. It does require customers to deposit at least $1,000 each month into their account.

## The best options

### The best savings account
```{r fasttrack-image, echo=FALSE}
#BOQ Fast Track Saver Image
knitr::include_graphics("https://themarketherald.com.au/wp-content/uploads/2019/11/BOQ-1.jpg")
```

The best savings account we found is the [Bank of Queensland Fast Track Saver Account](https://www.boq.com.au/personal/banking/savings-and-term-deposits/fast-track-saver-account).

It has a 2.15% interest rate for balances up to $250,000. It does require customers to deposit at least $1,000 each month into their account.

The BOQ Fast Track Saver Account is actually a set of two linked accounts, a savings account (Fast Track Saver) and a transaction account (Day2Day). There is no requirement that you use the linked transaction account other than to transfer money to and from the Bank of Queensland.

### If you don't like minimum deposit requirements
We generally don't mind minimum deposit requirements precisely because they are so easy to get around. It's always possible to send money back and forth between two bank accounts to meet a minimum deposit requirement. Nevertheless if you don't like the hassle of shuffling money around, there are still options available to you.

```{r me-logo, echo=FALSE, out.width= '33%'}
#ME bank logo
knitr::include_graphics("https://cdn.productreview.com.au/resize/listing-picture/d036dbb9-0014-3cec-8205-e4008604b58b?width=1000&height=1000&dpr=2&withoutEnlargement=true")
```

If you don't want a minimum deposit requirement, the best savings account is [ME Bank Online Saver Account](https://www.mebank.com.au/personal/bank-accounts/online-savings-account/). 

It provides a 2.05% interest rate, which is only 0.10% lower than our best savings account recommendation. On this account, you are required to make at least four Tap & Go transaction every month using the Debit Mastercard associated with an attached Everyday Transaction Account.

However, given that most people are likely to make at least one purchase using Tap & Go weekly, we consider this requirement easy to meet. 

## How we picked

We looked for savings accounts with the highest interest rate and the following criteria:

- no prohibitive conditions
- no limits on benefit periods (i.e. bonus interest rates for short periods of time)
- no restrictions on withdrawals
- no account keeping fees


### Prohibitive conditions
We looked out for any conditions that we considered prohibitive. We have seen bank accounts with fairly creative requirements and we aren't interested in banks that make us jump through too many hoops to get a good interest rate.

### Limits on benefit periods
Some banks provide bank accounts with bonus interest rates that last for a short while (e.g. 4 months). We specifically avoid these offers as we consider it a hassle to change savings account every couple of months. 

### Restrictions on withdrawals
Some savings accounts offer a higher interest rate if customers do not withdraw funds from a given account, or otherwise limit the number of withdrawals each month. We felt that such a setup defeats the purpose of a savings account and is more comparable to a term deposit. We therefore exlcuded any offers that had restrictions on withdrawals.

### Account keeping fees
We eliminate any offer that requires account keeping fees. Given the current selection of offerings, there is no reason to go with a bank that charges account keeping fees.

### Interest Rates
We then selected the account with the best interest rate.

## The competition
These are the accounts we looked at:

```{r savings-acct, echo=FALSE}
#This prints a table of the savings accounts we looked at
savings_acct_data <- read_excel(path = "input/bank_data.xlsx", sheet = "Savings Accounts") %>%
  filter(date == lubridate::dmy("01-12-19"))

savings_acct_data %>%
  arrange(desc(max_interest_rate)) %>%
  select(bank, acct_name, max_interest_rate, max_balance, monthly_deposit_rules, withdrawal_rules, conditions) %>%
  rename(Bank = bank,
         `Account Name` = acct_name,
         `Interest Rate` = max_interest_rate,
         `Maximum Balance` = max_balance, 
         `Mininum Monthly Deposit` = monthly_deposit_rules,
         `Withdrawal Restrictions` = withdrawal_rules,
         `Other conditions` = conditions) %>%
  kable(caption = "Savings Accounts",
        format.args = list(big.mark = ','))

```
